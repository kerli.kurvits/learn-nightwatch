var config = require('../../nightwatch.conf.js');

module.exports = { 
  'Nähtamatud loomad': function(browser) {
    browser
      .url('https://nahtamatudloomad.ee/')
      .waitForElementVisible('body')
      .windowSize('current', 1200, 700)
      .assert.title("Nähtamatud Loomad")
      .saveScreenshot(config.imgpath(browser) + 'nahtamatud_home.png')
      .assert.elementPresent('img.modal-image')
      .click('a.close-button')
      .pause(500)
      .saveScreenshot(config.imgpath(browser) + 'image_closed.png')
      .assert.containsText("header", "Pood")
      .useXpath()
      .click("//a[text()='Pood']")
      .pause(500)
      .useCss()
      .waitForElementVisible('body')
      .assert.title('Pood | Nähtamatud Loomad')
      .pause(500)
      .saveScreenshot(config.imgpath(browser) + 'pood.png')
      .assert.containsText("header", "Tegutse")
      .useXpath()
      .click("//a[text()='Tegutse']")
      .pause(1000)
      .useCss()
      .waitForElementVisible('body')
      .assert.title('Tegutse | Nähtamatud Loomad')
      .pause(1000)
      .saveScreenshot(config.imgpath(browser) + 'tegutse.png')
      .assert.containsText("div.container", "Kontakt")
      .end();
    }
};